function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

let data =[];
let dataNew = [];
let heights = [];

for (let index = 0; index < 100 ; index++) {
    data.push({
        height: getRandomInt(1460,1580),
        result: 'S',
    });
}

for (let index = 0; index < 50 ; index++) {
    data.push({
        height: getRandomInt(1580,1650),
        result: 'M',
    });
}

for (let index = 0; index < 50 ; index++) {
    data.push({
        height: getRandomInt(1650,1710),
        result: 'L',
    });
}

for (let index = 0; index < 30 ; index++) {
    data.push({
        height: getRandomInt(1710,1750),
        result: 'XL',
    });
}

for (let index = 0; index < 100 ; index++) {
    data.push({
        height: getRandomInt(1750,1900),
        result: 'XXL',
    });
}
for (let index = 0; index < data.length ; index++) {
    dataNew.push(data[getRandomInt(0,330)]);
}
console.log(JSON.stringify(dataNew));